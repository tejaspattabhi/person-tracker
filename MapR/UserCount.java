import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class UserCount{
	

	public static class Map extends Mapper<LongWritable, Text, Text, IntWritable>{
		
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			//from CDR_File
			
			String[] mydata = value.toString().split(",");
			
			String tempdata = mydata[1];
			tempdata = tempdata.toString().replaceAll("\"","");
			
		    if (mydata.length > 6){
			
				context.write(new Text(tempdata),new IntWritable(1));
							
			}	
					
		}
	
		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
		
		
		}
	}

	public static class Reduce extends Reducer<Text,IntWritable,Text,IntWritable> {
		
		public void reduce(Text key, Iterable<IntWritable> values,Context context ) throws IOException, InterruptedException {
			
					
			int count=0;
			for(IntWritable t : values){
				count++;
			}
			context.write(key,new IntWritable(count));
			
		}
	}

// Driver program
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		
//		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();		// get all args
		
		if (args.length != 2) {
			System.err.println("Usage: UserCount <in> <out>");
			System.exit(2);
		}
		
	
		Job job = new Job(conf, "UserCount");
		job.setJarByClass(UserCount.class);
		
		
	   
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		
		// set output key type 
		job.setOutputKeyClass(Text.class);
		
		// set output value type
		job.setOutputValueClass(IntWritable.class);
		
		//set the HDFS path of the input data
		FileInputFormat.addInputPath(job, new Path(args[0]));
		
		// set the HDFS path for the output 
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		//Wait till job completion
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}

	
	