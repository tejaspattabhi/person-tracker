import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class ServerFile{
	

	public static class Map extends Mapper<LongWritable, Text, Text, Text>{
		
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			//from CDR_File
			
			String[] mydata = value.toString().split(",");
			
			String tempkey = mydata[1]+","+mydata[2];
			
			tempkey = tempkey.replaceAll("\"","");
			
			if (mydata.length > 6){
			
				String tempdata = "\t"+mydata[3]+","+mydata[5]+","+mydata[6];
					
				tempdata = tempdata.replaceAll("\"","");
				
					context.write(new Text(tempkey),new Text(tempdata));
							
			}	
					
		}
	
		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
		
		
		}
	}

	public static class Reduce extends Reducer<LongWritable,Text,Text,ArrayWritable> {
		
		@SuppressWarnings("unchecked")
		public void reduce(Text key, Iterable<Text> values,Context context ) throws IOException, InterruptedException {
			
			ArrayList list = new ArrayList();
			
				
			for (Text text : values) {
				
				list.add(text);
						
			}
			
			context.write(key,new ArrayWritable((String[]) list.toArray()));
			
		}
	}

// Driver program
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		
//		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();		// get all args
		
		if (args.length != 2) {
			System.err.println("Usage: ServerFile <in> <out>");
			System.exit(2);
		}
		
	
		Job job = new Job(conf, "ServerFile");
		job.setJarByClass(ServerFile.class);
		
		
	   
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		
		// set output key type 
		job.setOutputKeyClass(Text.class);
		
		// set output value type
		job.setOutputValueClass(Text.class);
		
		//set the HDFS path of the input data
		FileInputFormat.addInputPath(job, new Path(args[0]));
		
		// set the HDFS path for the output 
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		//Wait till job completion
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}

	
	
