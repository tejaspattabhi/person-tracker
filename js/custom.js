$(function () {
	$("#map").hide ();
	
	var thisDate = new Date (2012, 6, 1, 0, 0, 0, 0);
	
	$('#thisDate').datepicker({
		format: 'mm-dd-yyyy'
	});
	
	$('#thisDate').val($.datepicker.formatDate('mm-dd-yy', thisDate));
	
	$('#personID').autocomplete({
		source: ['AAH03JAAQAAAO9VABf', 'AAH03JAAQAAAO9VABu', 'AAH03JAAQAAAO9VACH', 'AAH03JAAQAAAO9VADG', 'AAH03JAAQAAAO9VADY']
	});
	
	var map;

	function calcRoute (origin, destination) {
		var rendererOptions = {
			preserveViewport: true,         
			suppressMarkers: true
		};
		var directionsService = new google.maps.DirectionsService();

		var request = {
			origin: origin,
			destination: destination,
			travelMode: google.maps.TravelMode.DRIVING
		};

		var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
		directionsDisplay.setMap(map);

		directionsService.route(request, function(result, status) {
			console.log(result);

			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(result);
			}
		});
	}

	function initialize(locations) {	
		// locations needs to be made dynamic
		// var locations = [
			// ['Time', -33.890542, 151.274856, 4],
			// ['Coogee Beach', -33.923036, 151.259052, 5],
			// ['Cronulla Beach', -34.028249, 151.157507, 3],
			// ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
			// ['Maroubra Beach', -33.950198, 151.259302, 1]
		// ];

		var mapOptions = {
			zoom: 10,
			center: new google.maps.LatLng(23.797096, 90.401218),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById('map'), mapOptions);
		// directionsDisplay.setMap(map);
		
		var infowindow = new google.maps.InfoWindow();
		var markers = [];

		var label = "A";
		for (var i = 0; i < locations.length; i++) {  
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(parseFloat (locations[i][1]), parseFloat (locations[i][2])),
				map: map,
				title: locations[i][0],
				label: String.fromCharCode(label.charCodeAt(0) + i)
			});

			google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
				return function() {
					infowindow.setContent(locations[i][0]);
					infowindow.open(map, marker);
				}
			})(marker, i));
			
			google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
				return function() {
					infowindow.close ();
				}
			})(marker, i));
			
			markers.push (new google.maps.LatLng(parseFloat (locations[i][1]), parseFloat (locations[i][2])));
		}
		
		if (markers.length == 2) {
			calcRoute (markers[0], markers[1]);
		}
		else if (markers.length > 2) {
			for (var i = 0; i < markers.length - 1; i++) {  
				calcRoute (markers[i], markers[i + 1]);
			}
		}
	}

	
	$('#submit').bind("click", function(e){
			// console.log ('http://127.0.0.1:5623/?uid=' + $("#personID").val() + "&date=" + $("#thisDate").val())
			$.ajax ({
				'url': 'http://127.0.0.1:5623/?uid=' + $("#personID").val() + "&date=" + $("#thisDate").val(),
				'returnType': 'json',
				'success': function (data) {
					if (jQuery.isEmptyObject(data)) {
						$("#map").hide ();
						document.getElementById('error').innerHTML = '<div class="panel panel-danger">\
																	<div class="panel-heading">Error</div>\
																	<div class="panel-body">\
																	<h3>Data Unavailable!</h3>\
																	</div>';
					}						
					else if (data['error_info']) {
						$("#map").hide ();
						document.getElementById('error').innerHTML = '<div class="panel panel-danger">\
																	<div class="panel-heading">Error</div>\
																	<div class="panel-body"><h3>' + data["error_info"] + 
																	'</h3>\
																	</div>';
					}
					else {
						// take care of the data
						$("#map").show ();
						initialize (data);
					}
					
					$('html, body').stop().animate({
						scrollTop: $("#map").offset().top
					}, 1000);
					e.preventDefault();
				},
				'error': function (xhr, ajaxOptions, thrownError){
					console.log (thrownError);
					document.getElementById('error').innerHTML = '<div class="panel panel-danger">\
																	<div class="panel-heading">Error</div>\
																	<div class="panel-body">\
																	<h3>Data Unavailable!</h3>\
																	</div>';
				},
			   'timeout' : 15000 // timeout of the ajax call
			});
	});
});