##################################################################################
# Name			:	Server - Web Listener Component of Person Tracker
# Description		:	Listen's to port 5623, for two kinds of web requests
# 				GET: Request coming from a client/browser requesting
#                    	  data processed.
# Function		: 	Act as the Web Listener to MapReduced CDR data
# Author		: 	Tejas Tovinkere Pattabhi
# Last Updated	: 	08/06/2015
##################################################################################

from BaseHTTPServer import BaseHTTPRequestHandler
import urlparse
import json
import datetime
import io

# Data is the dictionary where the aggregated data from webhooks are stored. It acts like a hashmap
data = {}

# GetHandler is the class overriding the methods for GET and POST requests to perform the described actions
class GetHandler(BaseHTTPRequestHandler):
	# do_GET is the method which is executed when there is a request from client. 
	# Function: Posting Data as JSON, clearing a Tenant's/all data
	def do_GET(self):
		global data
		parsed_path = urlparse.urlparse(self.path)
		
		message = '<html><head><title>Unknown Request</title></head><body><h1>I\'ve no idea what you are talking about!</h1></body></html>'
		try:
			params = dict([p.split('=') for p in (parsed_path.query).split('&')])
			print params
		except:
			params = {}
		
		if params != {}:
			if params["uid"] in data:
				if params["date"] in data[params["uid"]]:
					toSend = []
					for tS in data[params["uid"]][params["date"]]:
						toSend.append ([tS] + data[params["uid"]][params["date"]][tS])
					print toSend
					message = json.dumps (toSend)
				else:
					message = json.dumps ({'error_info': 'Date Not Found'})
			else:
				message = json.dumps ({'error_info': 'UID Not Found!'})
				
	
		# These are the headers which are required to return the request as JSON
		self.send_response(200)
		self.send_header("Access-Control-Allow-Origin", "*")
		self.send_header('Content-Type', 'application/json')
        
		self.end_headers()
		self.wfile.write(message)
		return
		
	def do_POST(self):
		global data

		# do Nothing as no post requests will be seen
		return

if __name__ == '__main__':
    	# Setup a basic HTTP server
	from BaseHTTPServer import HTTPServer

	# Not mentioning the hostname/IP address makes it bind to the local machine.
	server = HTTPServer(('', 5623), GetHandler)
	print 'Starting server, use <Ctrl-C> to stop'
	print 'Initializing Data..'
	
	# Initialize MapR data
	with open("./data/part-r-00000") as file:
		for line in file:
			KVPair = line.split("\t")
			KVPair = [x for x in KVPair if x != '']
			KVPair = [x.strip('\n') for x in KVPair]
			
			UID = KVPair[0].split(',')[0]
			thisDate = KVPair[0].split(',')[1]
			timeStamp = KVPair[1].split(',')[0]
			latitude = KVPair[1].split(',')[1]
			longitude = KVPair[1].split(',')[2]
			date = datetime.datetime.strptime(thisDate + " " + timeStamp, '%Y%m%d %H:%M:%S')
			
			if UID in data:
				if date.strftime("%m-%d-%Y") in data[UID]:
					if date.strftime("%H:%M:%S") in data[UID][date.strftime("%m-%d-%Y")]:
						print "Duplicate Data"
					else:
						data[UID][date.strftime("%m-%d-%Y")][date.strftime("%H:%M:%S")] = [latitude, longitude]
				else:
					data[UID][date.strftime("%m-%d-%Y")] = {}
					data[UID][date.strftime("%m-%d-%Y")][date.strftime("%H:%M:%S")] = [latitude, longitude]
			else:
				data[UID] = {}
				data[UID][date.strftime("%m-%d-%Y")] = {}
				data[UID][date.strftime("%m-%d-%Y")][date.strftime("%H:%M:%S")] = [latitude, longitude]

	# with open ('test.json', 'w') as ofile:
		# json.dump(data, ofile)
		
	print "Data initialized. Now ready to serve."
	server.serve_forever()
