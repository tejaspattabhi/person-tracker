import io
import json

data = {}

with open ("test.txt") as file:
	for line in file:
		freq = line.split('\t')
		freq = [x for x in freq if x != '']
		freq = [x.strip('\n') for x in freq]
		
		if len (data) < 10:
			if freq[0] in data:
				data[freq[0]] += int (freq[1])
			else:
				data[freq[0]] = int (freq[1])
		else:
			for id in data:
				if data[id] < int (freq[1]):
					if not (freq[0] in data):
						del data[id]
						data[freq[0]] = int (freq[1])
			
print json.dumps (data, sort_keys=True, indent=4)
with open ("output", 'w') as ofile:
	for id in data:
		ofile.write (id + "," + str (data[id]) + "\n")
	ofile.close ()
